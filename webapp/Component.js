(function () {
	"use strict";
	/*global sap, jQuery */

	/**
	 * @fileOverview Application component to display information on entities from the GWSAMPLE_BASIC
	 *   OData service.
	 * @version @version@
	 */
	jQuery.sap.declare("sap.cu.ovp.fmco01.Z_CU_OVP_FMCO.Component");

	jQuery.sap.require("sap.ovp.app.Component");

	sap.ovp.app.Component.extend("sap.cu.ovp.fmco01.Z_CU_OVP_FMCO.Component", {
		metadata: {
			manifest: "json"
		}
	});
}());